import $ from 'jquery';

export default function initTitle(menuSelection, title) {
  // Name
  if (typeof title == 'string') $('div#nav').text(title);

  // Menu Selection
  if (typeof menuSelection == 'string') $(document).ready(() => {
    // Menu bar (desktop only)
    const items = document.querySelectorAll('div#menu > div#navbar > ul > li > a');
    for (const item of items) {
      if (item.innerHTML == menuSelection) item.parentElement.classList.add('always-on');
    }
  });
}
