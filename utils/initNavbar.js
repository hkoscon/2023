import $ from 'jquery';

function setupNavbar() {
  $.fn.menumaker = function (options) {
    const navbar = $(this);
    const settings = $.extend({
      title: '',
      format: 'dropdown',
      sticky: false,
    }, options);

    return this.each(function () {
      navbar.prepend(`<div id="menu-button">${settings.title}</div>`);
      $(this).find('#menu-button').on('click', function () {
        const mainmenu = $(this).next('ul');
        $(this).toggleClass('menu-opened');
        if (mainmenu.hasClass('open')) {
          mainmenu.hide().removeClass('open');
        } else {
          mainmenu.show().addClass('open');
          if (settings.format === 'dropdown') {
            mainmenu.find('ul').show();
          }
        }
      });

      navbar.find('li ul').parent().addClass('has-sub');

      const multiTg = function () {
        navbar.find('.has-sub').prepend('<span class="submenu-button"></span>');
        navbar.find('.submenu-button').on('click', function () {
          $(this).toggleClass('submenu-opened');
          if ($(this).siblings('ul').hasClass('open')) {
            $(this).siblings('ul').removeClass('open').hide();
          } else {
            $(this).siblings('ul').addClass('open').show();
          }
        });
      };

      if (settings.format === 'multitoggle') multiTg();
      else navbar.addClass('dropdown');

      if (settings.sticky === true) navbar.css('position', 'fixed');

      const resizeFix = function () {
        if ($(window).width() > 700) {
          navbar.find('ul').show();
        }

        if ($(window).width() <= 700) {
          navbar.find('ul').hide().removeClass('open');
          navbar.find('div#menu-button').removeClass('menu-opened');
          navbar.find('span.submenu-button').removeClass('submenu-opened');
        }
      };
      resizeFix();
      return $(window).on('resize', resizeFix);
    });
  };
}

/**
 * doubleTapToGoDecorator
 * Adds the ability to remove the need for a second tap
 * when in the mobile view
 *
 * @param {function} f - doubleTapToGo
 */
function doubleTapToGoDecorator(f) {
  return function () {
    this.each(function () {
      $(this).on('click', (e) => {
        // If mobile menu view
        if ($('#menu-button').css('display') == 'block') {
          // If this is not a submenu button
          if (!$(e.target).hasClass('submenu-button')) {
            // Remove the need for a second tap
            window.location.href = $(e.target).attr('href');
          }
        }
      });
    });
    return f.apply(this);
  };
}

/*
  By Osvaldas Valutis, www.osvaldas.info
  Available for use under the MIT License
*/
function setupMobile(window, document) {
  $.fn.doubleTapToGo = function () {
    if (!('ontouchstart' in window) && !navigator.msMaxTouchPoints && !navigator.userAgent.toLowerCase().match(/windows phone os 7/i)) return false;

    this.each(function () {
      let curItem = false;

      $(this).on('click', function (e) {
        const item = $(this);
        if (item[0] != curItem[0]) {
          e.preventDefault();
          curItem = item;
        }
      });

      $(document).on('click touchstart MSPointerDown', (e) => {
        let resetItem = true;
        const parents = $(e.target).parents();

        for (let i = 0; i < parents.length; i++) if (parents[i] == curItem[0]) resetItem = false;

        if (resetItem) curItem = false;
      });
    });
    return this;
  };
}

export default function initNavbar() {
  setupNavbar();
  setupMobile(window, document);

  // Add decorator to the doubleTapToGo plugin
  $.fn.doubleTapToGo = doubleTapToGoDecorator($.fn.doubleTapToGo);

  $(document).ready(() => {
    $('#navbar').menumaker({
      title: '',
      format: 'multitoggle',
    });
    $('#navbar li:has(ul)').doubleTapToGo();
  });
}
