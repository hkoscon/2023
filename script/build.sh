#!/usr/bin/env bash

set -xe

if [ -f .env ]; then
  . .env
fi

mkdir -p static/data

# Note: added --insecure to fix GitLab CI issue with Let's Encrypt cert
curl --insecure -o static/data/timetable.json $TIMETABLE_URL
curl --insecure -o static/data/event_info.json $EVENT_INFO_URL
NUXT_MODE=generate yarn generate
